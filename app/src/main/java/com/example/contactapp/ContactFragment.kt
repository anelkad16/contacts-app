package com.example.contactapp

import android.annotation.SuppressLint
import android.content.ContentResolver
import android.net.Uri
import android.provider.ContactsContract
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide

abstract class ContactFragment(
    @LayoutRes layout: Int
) : Fragment(layout) {

    protected val items = mutableListOf<ContactInfo>()

    protected fun loadContacts() {
        items.clear()
        val contentResolver = requireActivity().contentResolver
        val cursor = contentResolver.query(
            ContactsContract.Contacts.CONTENT_URI,
            null,
            null,
            null,
            ContactsContract.Contacts.DISPLAY_NAME + " ASC"
        )
        cursor?.let {
            if (it.moveToFirst()) {
                do {
                    val contactId = it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts._ID))
                    val name = it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
                    val phoneNumber = getPhoneNumber(contentResolver, contactId)
                    val avatarUri = it.getString(it.getColumnIndexOrThrow(ContactsContract.Contacts.PHOTO_URI))
                    val contactInfo = ContactInfo(
                        id = contactId.toLong(),
                        picture = if (avatarUri != null) Uri.parse(avatarUri) else null,
                        name = name,
                        phoneNumber = phoneNumber
                    )
                    items.add(contactInfo)
                } while (it.moveToNext())
            }
            it.close()
        }
    }

    private fun getPhoneNumber(contentResolver: ContentResolver, contactId: String): String {
        val cursor = contentResolver.query(
            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
            null,
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
            arrayOf(contactId),
            null
        )
        var phoneNumber = ""
        cursor?.let {
            if (it.moveToFirst()) {
                phoneNumber = it.getString(it.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER))
            }
            it.close()
        }
        return phoneNumber
    }
}