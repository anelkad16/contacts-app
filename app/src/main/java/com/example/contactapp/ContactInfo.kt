package com.example.contactapp

import android.net.Uri

data class ContactInfo(
    val id: Long,
    val picture: Uri?,
    val name: String,
    val phoneNumber: String
)