package com.example.contactapp.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import com.example.contactapp.ContactInfo

interface OnClickAction {

    fun onClick(contact: ContactInfo, context: Context) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.withAppendedPath(
            ContactsContract.Contacts.CONTENT_URI,
            contact.id.toString()
        )
        context.startActivity(intent)
    }

}