package com.example.contactapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.ContactInfo
import com.example.contactapp.R

class DataItemAdapter(
    private val mDataItems: List<ContactInfo>,
    private val mContext: Context
) :
    RecyclerView.Adapter<DataItemAdapter.ViewHolder>(),
    OnClickAction {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = LayoutInflater.from(parent.context)
            .inflate(R.layout.contacts_list_item, parent, false)
        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int = mDataItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contactInfo = mDataItems[position]
        holder.textViewName.text = contactInfo.name
        holder.textViewPhoneNumber.text = contactInfo.phoneNumber
        if (contactInfo.picture != null ) {
            holder.imageView.setImageURI(contactInfo.picture )
        } else {
            holder.imageView.setImageResource(R.drawable.baseline_person_24)
        }
        holder.mView.setOnClickListener { onClick(contactInfo, mContext) }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val imageView: ImageView
        val textViewName: TextView
        val textViewPhoneNumber: TextView
        val mView: View

        init {
            imageView = view.findViewById(R.id.imageView)
            textViewName = view.findViewById(R.id.name_text_item)
            textViewPhoneNumber = view.findViewById(R.id.number_text_item)
            mView = itemView
        }
    }

}