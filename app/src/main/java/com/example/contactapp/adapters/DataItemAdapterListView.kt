package com.example.contactapp.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.contactapp.ContactInfo
import com.example.contactapp.R

class DataItemAdapterListView(
    context: Context,
    objects: MutableList<ContactInfo>
) :
    ArrayAdapter<ContactInfo>(context, R.layout.contacts_list_item, objects),
    OnClickAction {

    private val mDataItems: List<ContactInfo>
    private val mInflater: LayoutInflater

    init {
        mDataItems = objects
        mInflater = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var itemView = convertView
        if (itemView == null) {
            itemView = mInflater.inflate(R.layout.contacts_list_item, parent, false)
        }

        val contactInfo = mDataItems[position]

        val imageView = itemView!!.findViewById<ImageView>(R.id.imageView)
        if (contactInfo.picture != null ) {
            imageView.setImageURI(contactInfo.picture )
        } else {
            imageView.setImageResource(R.drawable.baseline_person_24)
        }

        val nameTextView = itemView.findViewById<TextView>(R.id.name_text_item)
        nameTextView.text = contactInfo.name

        val phoneNumberTextView = itemView.findViewById<TextView>(R.id.number_text_item)
        phoneNumberTextView.text = contactInfo.phoneNumber

        itemView.setOnClickListener { onClick(contactInfo, context) }

        return itemView
    }

}