package com.example.contactapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import com.example.contactapp.databinding.ActivityMainNavBinding
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityMainNavBinding
    private val drawerLayout by lazy { binding.drawerLayout }
    private val listViewFragment = ListViewFragment()
    private val recyclerViewFragment = RecyclerViewFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainNavBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(drawerLayout.findViewById(R.id.toolbar))
        binding.navView.setNavigationItemSelectedListener(this)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            binding.drawerLayout.findViewById(R.id.toolbar),
            R.string.open_nav_drawer,
            R.string.close_nav_drawer
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val permission = Manifest.permission.READ_CONTACTS
        if (ContextCompat.checkSelfPermission(
                this,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            when (item.itemId) {
                R.id.action_recycle_view -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_container, recyclerViewFragment)
                        .commit()
                }

                R.id.contact_list_view -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_container, listViewFragment)
                        .commit()
                }
            }
        } else {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission),
                READ_CONTACTS_PERMISSION_REQUEST
            )
        }
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    companion object {
        private const val READ_CONTACTS_PERMISSION_REQUEST = 1
    }

}