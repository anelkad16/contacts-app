package com.example.contactapp

import android.os.Bundle
import android.view.View
import android.widget.ListView
import com.example.contactapp.adapters.DataItemAdapterListView

class ListViewFragment :
    ContactFragment(R.layout.fragment_list_view) {

    private lateinit var adapter: DataItemAdapterListView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = DataItemAdapterListView(requireContext(), items)
        val listView = view.findViewById<ListView>(R.id.contact_list_view)
        listView.adapter = adapter
        loadContacts()
    }

}