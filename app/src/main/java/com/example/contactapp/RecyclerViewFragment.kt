package com.example.contactapp

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.contactapp.adapters.DataItemAdapter

class RecyclerViewFragment :
    ContactFragment(R.layout.fragment_recycler_view) {

    private lateinit var adapter: DataItemAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = DataItemAdapter(items, requireContext())
        val recyclerView = view.findViewById<RecyclerView>(R.id.contact_recycler_view)
        recyclerView.adapter = adapter
        loadContacts()
    }

}